# FW Loading internals

The process for loading FW now is in two parts.  The first is setting up the UIO devices via dtbo files and the updated address table. 
The second is loading the FW via an SVF file to the FPGAs.

## UIO Daemon
The [uio-daemon](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/tree/develop) is a 
Linux Daemon on the Apollo Zynq that automatically loads requested dtbo files, validates them, and then generates a new top level address table for the FWs specified. 
All other Daemons that use uHAL or UIOHAL depend on this daemon must have this daemon in its "Requires" list.

This daemon serves as an API which listens on a UNIX socket, `/tmp/uio_socket`. It will listen for incoming directives from a UIO client (see below),
and will perform operations accordingly. Communication with this UIO server can be interactively done by using the `uio_send.py` Python3 script
located in the daemon repository, and installed under `/opt/uio-daemon`. The instructions for this will be shown in the next section.

The daemon is configured via the file /etc/uio_daemon. This is a YAML file with several control options:

```yaml
# FW/overlays from these directories will be loaded on boot
load_on_boot:
  - "/fw/SM"
  - "/fw/CM/CornellCM_MCU"

# Directory where the merged address table will be saved
address_table_path: "/opt/address_table"

# Xilinx Virtual Cable server to program FPGA
plxvc: "PLXVC.XVC_1"

# FPGA power up wait time (in seconds)
cmpwrup_timeout: 5
```

* **load_on_boot:** Specifies the list of directories for which the DTBO files will be loaded automatically on boot, when the daemon starts.
* **address_table_path:** Path to the directory where the address table (`address_apollo.xml`) is located.
* **plxvc:** The XVC (Xilinx Virtual Cable) server to use while programming the FPGA when `program-fpga` directive is invoked (see below).
* **cmpwrup_timeout:** Timeout in seconds when trying to power up the Command Module when `cmpwrup` directive is invoked (see below). 

## UIO Client

To interact with the UIO server described in the previous section, a UIO client script is introduced, which is called `uio_send.py`. Given that
`/opt/uio-daemon` is included in the `PATH` on a blade, this can be executed from anywhere on the Apollo blade.

`uio_send.py` understand the following directives (you can also see `uio_send.py --help`):

* get-uid
* load-tar
* unload-tar
* cmpwrup
* program-fpga
* list-uids
* list-devices

The aim of each directive will be explained below in the context of loading and unloading CM firmware.

### Loading CM Firmware: In One Go

For loading CM firmware, typical steps are to load the DTBO files, power up the CM and finally, program the FPGA. 
With `uio_send.py`, all of this can be
achieved with `program-fpga` directive, if a tarball file with DTBO files and an SVF file is passed:

```bash
uio_send.py program-fpga -f /path/to/firmware.tar.gz
```

This operation will ask for a client ID from the server to do the operation, and the set of DTBO files will be assigned to this unique
client ID. Please note that **you will need to pass this client ID** to `uio_send.py` if you wish to unload those DTBO files.

You can check the list of UIO devices or list of client IDs who loaded firmware by the following directives:

```bash
# List the client IDs that loaded FW
uio_send.py list-uids

# List UIO devices, and which client ID loaded them
# "device-tree" means that UIO devices were automatically loaded on boot by the server
uio_send.py list-devices
```

### Loading CM Firmware: In Separate Steps

Loading CM firmware can also be done in separate steps:

```bash
# Load the DTBO files, take note of the client ID from this operation!
uio_send.py load-tar -f /path/to/firmware.tar.gz

# Power up the CM
uio_send.py cmpwrup -u <clientID>

# Program the FPGA
uio_send.py program-fpga -u <clientID>
```

It can be noted that if `program-fpga` directive gets passed a client ID (and no tarball), it will look for a tarball
that was loaded previously **by the same client ID**. Hence, the operation will be an error if no tarball was previously
loaded, or that tarball does not have an SVF file.

### Unloading CM DTBO Files

Given the client ID from previous operations (can be checked via `list-uids` or `list-devices` as well), DTBO files can be
unloaded using `unload-tar` directive:

```bash
uio_send.py unload-tar -u <clientID>
```

After this is executed, you can again check the list of UIO devices and/or client IDs:

```bash
# After unload-tar, your client ID and the corresponding UIO devices should not be listed 
uio_send.py list-uids
uio_send.py list-devices
```

## Developing With UIOClient

Apart from interactive interfaces to the server from the command line, the `UIOClient` class (developed in C++, code is 
[here](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/blob/develop/uio_client_cpp/uio_client.hh))
can be used to interact with the UIO server from other software, which links against the `UIOClient` shared library 
(`-lUIOClient` while building). Python bindings for the `UIOClient` also exist and installed in Apollo blades, so it can 
also be used in Python3 applications. Each case will be explained below.

### C++

An example C++ code which uses `UIOClient` class and its methods can be found [here](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/blob/develop/uio_client_cpp/uio_client_example.cc).
Typically, the workflow looks like this:

```cpp
#include <iostream>
#include "uio_client.hh"

int main() {
  UIOStatus status;
  UIOClient client = UIOClient("/tmp/uio_socket");

  /* Ask for a client ID from the server, and retain it in this instance of UIOClient for further ops. */ 
  status = client.Init();
  if (status != UIOStatus::OK) {
      std::cout << "Failed to obtain a UID for the client" << std::endl;
      return -1;
  }

  /* If all OK, proceed to load a tarball. */
  status = client.SendLoadTarMessage("/path/to/tarball.tar.gz");
  if (status != UIOStatus::OK) {
      std::cout << "Failed to load DTBO files." << std::endl;
      return -1;
  }

  /* Get mapping of UIO device -> client ID for the devices that are loaded. */
  std::map<std::string, std::string> deviceMap;
  status = client.GetUIODevices(deviceMap);
  
  /* If all OK, deviceMap now has the mapping. */ 

  return 0;
}
```

The definition of `UIOStatus` enum can be found [here](https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon/-/blob/develop/uio_client_cpp/uio_client.hh#L38-46),
which allows developers to easily keep track of their operations and what might have gone wrong.

### Python3

Thanks to Python bindings of `UIOClient`, it can also be used in Python3 applications. The function signatures are exactly the same:

```python
import sys
from UIOClient import UIOClient, UIOStatus

client = UIOClient("/tmp/uio_socket")

# Get a client ID
status = client.Init()
if status != UIOStatus.OK:
  print("Could not get a client ID from server.")
  sys.exit(1)

# If all OK, load a tarball
status = client.SendLoadTarMessage("/path/to/tarball.tar.gz")
if status != UIOStatus.OK:
  print("Failed to load DTBO files.")
  sys.exit(1)

# Get mapping of UIO device -> client ID
devices = {}
status = client.GetUIODevices(devices)

# The input dictionary will be modified with the information you want!
if status == UIOStatus.OK:
  print(devices)
```


# OLD

### Loading Firmware
To load CM FPGA firmware from the SM, three things must be done

    1. Update the dtbo files for the AXI slaves in the CM FW (requires re-boot)    
    2. Update the address table files for the CM's AXI slaves    
    3. Program the CM FPGAs via BUTool or xvc


#### DTBO Files
Device-tree overlay files (.DTBO) contain device-tree entries for the AXI slaves in the CM and are in a format that they can be dynamically added to the device-tree at runtime.
Since the number and addresses of AXI slaves should remain relatively constant, the dtbo files are loaded on boot by linux and therefor require a restart if they are changed. 
The 2.X (tag soon) versions of the CM FW will automatically generate the dtbo files for the CM FW and are placed in kernel/hw/dtbo .
These files should be copied to the directory /fw/dtbo/dtbo on the SM before rebooting.  Please remove all un-needed dtbo files from that directory.
After a re-boot you should see lines similar to these from dmesg
```
[    7.593868] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/axiSlaveCM_K_INFO
[    7.605136] uio_pdrv_genirq b1003000.CM_K_INFO: IRQ index 0 not found
[    7.637947] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/axiSlaveCM_V_INFO
[    7.649276] uio_pdrv_genirq b3003000.CM_V_INFO: IRQ index 0 not found
[    7.679551] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/axiSlaveKINTEX_IPBUS
[    7.691034] uio_pdrv_genirq b0000000.KINTEX_IPBUS: IRQ index 0 not found
[    7.713378] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/axiSlaveKINTEX_SYS_MGMT
[    7.725199] uio_pdrv_genirq b1001000.KINTEX_SYS_MGMT: IRQ index 0 not found
[    7.742854] OF: overlay: WARNING: memory leak will occur if overlay removed, property: /__symbols__/axiSlaveK_C2C_PHY
[    7.754164] uio_pdrv_genirq b1000000.K_C2C_PHY: IRQ index 0 not found
```
The IRQ messages are present because the example AXI slaves do not have an interrupt connected. 
The memory leak warning can be ignored since we only load/unload these slaves during boot/shutdown.

### Address Tables
The address table for the system is located in  /opt/address_table.
A final system for merging SM and CM slaves into the same file is in development, but for now they need to be manually merged. 
The SM build generates the files connections.xml and address_apollo.xml that load the SM slaves only (under the uHAL name test.0)
The current procedure for merging the SM and CM address tables is to make copies(connetions_SM_CM.xml \& address_apollo_SM_CM.xml) and use/update those. 
The connections file simply has to point to the address_apollo_SM_CM.xml file:
```
<?xml version="1.0" encoding="UTF-8"?>
<connections>
  <!-- be sure to use the same file in both "uri" and "address_table" -->
  <connection id="test.0"        uri="uioaxi-1.0:///opt/address_table/address_apollo_SM_CM.xml"                     address_table="file:///opt/address_table/address_apollo_SM_CM.xml" />
  <connection id="SM_CM"         uri="uioaxi-1.0:///opt/address_table/address_apollo_SM_CM.xml"                     address_table="file:///opt/address_table/address_apollo_SM_CM.xml" />

</connections>
```
The address_apollo_SM_CM.xml file is just the address_apollo.xml file with the contents of the address_CM_REV_NAME.xml appended to it. 

##### address_Cornell_rev1_p2_VU7p-2-SM_7s.xml
```
<node id="TOP">
  <node id="VIRTEX_SYS_MGMT"  address="0xc0000000"  module="file://modules_Cornell_rev1_p2_VU7p-2-SM_7s/VIRTEX_SYS_MGMT.xml"                  />
  <node id="V_IO"             address="0xc1000000"  module="file://modules_Cornell_rev1_p2_VU7p-2-SM_7s/CM_IO.xml"                     />
  <node id="CM_V_INFO"        address="0xc2000000"  module="file://modules_Cornell_rev1_p2_VU7p-2-SM_7s/FW_INFO.xml"                   />
  <node id="VIRTEX_IPBUS"     address="0xc5000000"  module="file://modules_Cornell_rev1_p2_VU7p-2-SM_7s/IPBUS.xml"                     />
  <node id="V_C2C_PHY"        address="0xc6000000"  module="file://modules_Cornell_rev1_p2_VU7p-2-SM_7s/DRP_USP_GTY.xml"                  />
</node>
```
##### address_apollo.xml
```
<node id="TOP">
  <node id="CM"                      address="0x03000000"  module="file://modules/CM_USP.xml"                    />
  <node id="SERV"                    address="0x04000000"  module="file://modules/SERV_rev2.xml"                  />
  <node id="SLAVE_I2C"               address="0x05000000"  module="file://modules/SLAVE_I2C.xml"                  />
  <node id="PLXVC"                   address="0x06000000"  module="file://modules/plXVC_rev2.xml"                  />
  <node id="SM_INFO"                 address="0x0a000000"  module="file://modules/FW_INFO.xml"                   />
  <node id="MONITOR"                 address="0x0b000000"  module="file://modules/MONITOR_USP.xml"                  />
  <node id="XVC_LOCAL"               address="0x0c000000"  module="file://modules/XVC.xml"                       />
  <node id="C2C1_AXI_FW"          address="0x0d000000"  module="file://modules/C2C1_AXI_FW.xml"                  />
  <node id="C2C1_AXILITE_FW"      address="0x0e000000"  module="file://modules/C2C1_AXILITE_FW.xml"                  />
  <node id="MEM_TEST"                address="0x0f000000"  module="file://modules/MEM_TEST.xml"                  />
  <node id="C2C2_AXI_FW"      address="0x13000000"  module="file://modules/C2C2_AXI_FW.xml"                  />
  <node id="C2C2_AXILITE_FW"  address="0x14000000"  module="file://modules/C2C2_AXILITE_FW.xml"                  />
  <node id="PL_MEM"                  address="0x15000000"  module="file://modules/PL_MEM.xml"                    />
  <node id="C2C1_PHY"             address="0x19000000"  module="file://modules/DRP_7_GTX.xml"                  />
  <node id="C2C2_PHY"         address="0x1b000000"  module="file://modules/DRP_7_GTX.xml"                  />
  <node id="AXI_MON"                 address="0x1f000000"  module="file://modules/AXI_MONITOR.xml"                  />
  <node id="INT_AXI_FW"              address="0x20000000"  module="file://modules/INT_AXI_FW.xml"                  />
</node>
```
##### address_apollo_SM_CM.xml  (SM + CM1 + CM2)
```
<node id="TOP">
  <node id="CM"               address="0x03000000"  module="file://modules/CM_USP.xml"                    />
  <node id="SERV"             address="0x04000000"  module="file://modules/SERV_rev2.xml"                  />
  <node id="SLAVE_I2C"        address="0x05000000"  module="file://modules/SLAVE_I2C.xml"                  />
  <node id="PLXVC"            address="0x06000000"  module="file://modules/plXVC_rev2.xml"                  />
  <node id="SM_INFO"          address="0x0a000000"  module="file://modules/FW_INFO.xml"                   />
  <node id="MONITOR"          address="0x0b000000"  module="file://modules/MONITOR_USP.xml"                  />
  <node id="XVC_LOCAL"        address="0x0c000000"  module="file://modules/XVC.xml"                       />
  <node id="C2C1_AXI_FW"      address="0x0d000000"  module="file://modules/C2C1_AXI_FW.xml"                  />
  <node id="C2C1_AXILITE_FW"  address="0x0e000000"  module="file://modules/C2C1_AXILITE_FW.xml"                  />
  <node id="MEM_TEST"         address="0x0f000000"  module="file://modules/MEM_TEST.xml"                  />
  <node id="C2C2_AXI_FW"      address="0x13000000"  module="file://modules/C2C2_AXI_FW.xml"                  />
  <node id="C2C2_AXILITE_FW"  address="0x14000000"  module="file://modules/C2C2_AXILITE_FW.xml"                  />
  <node id="PL_MEM"           address="0x15000000"  module="file://modules/PL_MEM.xml"                    />
  <node id="C2C1_PHY"         address="0x19000000"  module="file://modules/DRP_USP_GTH.xml"                  />
  <node id="C2C2_PHY"         address="0x1b000000"  module="file://modules/DRP_USP_GTH.xml"                  />
  <node id="AXI_MON"          address="0x1f000000"  module="file://modules/AXI_MONITOR.xml"                  />
  <node id="INT_AXI_FW"       address="0x20000000"  module="file://modules/INT_AXI_FW.xml"                  />
  <node id="KINTEX_SYS_MGMT"  address="0x80000000"  module="file://modules_Cornell_rev1_p1_KU15p-2-SM_USP/KINTEX_SYS_MGMT.xml"                  />
  <node id="K_IO"             address="0x81000000"  module="file://modules_Cornell_rev1_p1_KU15p-2-SM_USP/CM_IO.xml"                     />
  <node id="CM_K_INFO"        address="0x82000000"  module="file://modules_Cornell_rev1_p1_KU15p-2-SM_USP/FW_INFO.xml"                   />
  <node id="KINTEX_IPBUS"     address="0x85000000"  module="file://modules_Cornell_rev1_p1_KU15p-2-SM_USP/IPBUS.xml"                     />
  <node id="K_C2C_PHY"        address="0x86000000"  module="file://modules_Cornell_rev1_p1_KU15p-2-SM_USP/DRP_USP_GTY.xml"                  />
  <node id="VIRTEX_SYS_MGMT"  address="0xc0000000"  module="file://modules_Cornell_rev1_p2_VU7p-1-SM_USP/VIRTEX_SYS_MGMT.xml"                  />
  <node id="V_IO"             address="0xc1000000"  module="file://modules_Cornell_rev1_p2_VU7p-1-SM_USP/CM_IO.xml"                     />
  <node id="CM_V_INFO"        address="0xc2000000"  module="file://modules_Cornell_rev1_p2_VU7p-1-SM_USP/FW_INFO.xml"                   />
  <node id="VIRTEX_IPBUS"     address="0xc5000000"  module="file://modules_Cornell_rev1_p2_VU7p-1-SM_USP/IPBUS.xml"                     />
  <node id="V_C2C_PHY"        address="0xc6000000"  module="file://modules_Cornell_rev1_p2_VU7p-1-SM_USP/DRP_USP_GTY.xml"                  />
</node>
```
The address_CM_REV_NAME.xml file is located along with its included xml files in os/address_table in the CM build. 
