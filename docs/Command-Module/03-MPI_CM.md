# MPI Command Module

Detailed documentation for the MPI CM is
available [**here**](https://mdttp.docs.cern.ch/) but does require
ATLAS internal authentication to access.  Prototype version 1, shown
below, will be fabricated in late 2022.

![Prototype version 1](figs/CM_prot_v1_web.png)