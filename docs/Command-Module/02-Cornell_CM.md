# Cornell Command Module

The Cornell Command Module manual is located [here](https://apollo-lhc.gitlab.io/cornell-cm/Manual/)

## Important note
The 7-series and USP Zynq SoCs have different memory mappings for AXI slaves, so you must choose the correct SM variant of the CM FW when building. 
Each CM FW has two versions, one with "-SM_7s" and one with "-SM_USP".

## Standard FW tarball (to be automatically used soon.)
A tar.gz file named after the CM build will contain the svf file, the dtbo files, and the needed address table files. 
Example: Cornell_rev1_p2_VU7p-1-SM_USP.tar.gz

+ bit/top_Cornell_rev1_p2_VU7p-1-SM_USP.svf
+ dtbo/*.dtbo
+ address_table/address_apollo.xml
+ address_table/connections.xml (ignore)
+ address_table/modules/*.xml

#### Address table use
If you want this by default in BUTool, modify /etc/BUTool to point to the address_apollo_SM_CM.xml file.
Make sure to either use the default "test.0" name in the connections file or add the name to the /etc/BUTool config file
####/etc/BUTool
```
#For BUTool.cxx
DEFAULT_ARGS=ApolloSM /opt/address_table/connections_SM_CM.xml SM_CM
lib=/opt/BUTool/lib/libBUTool_ApolloSMDevice.so
lib=/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so
```

### Programming with BUTool
```
[root@apollo06 ~]# BUTool.exe -a /opt/address_table/connections_SM_CM.xml SM_CM
Registered device: APOLLOSM
Registered device: GENERICIPBUS
Using .xml connection file...
>cmpwrup 
CM 1 is powered up
>svfplayer /fw/CM/top_Cornell_rev1_p1_KU15p-2-SM_7s.svf PLXVC.XVC_1
Found UIO labeled PLXVC @ /dev/uio14
Reading svf file...
>svfplayer /fw/CM/top_Cornell_rev1_p2_VU7p-1-SM_7s.svf PLXVC.XVC_1
Found UIO labeled PLXVC @ /dev/uio14
Reading svf file...
>write C2C1_AXILITE_FW.UNBLOCK 
Mask write to C2C1_AXILITE_FW.UNBLOCK
>write C2C2_AXILITE_FW.UNBLOCK
Mask write to C2C2_AXILITE_FW.UNBLOCK
>status 1 FIRMWARE
Process done
         FIRMWARE|  CM_K_INFO|  CM_V_INFO|    SM_INFO|
               --|-----------|-----------|-----------|
       BUILD_DATE| 0x20210804| 0x20210804| 0x20210806|
       BUILD_TIME| 0x  154150| 0x  145815| 0x  145238|

SW VER: -1


```
## CM->Zynq Monitoring UART
These are all the registers that deal with CM1's UART monitoring interface. (CM2's interface would be similar)
```
>nodes CM.CM_1.MONITOR*
    1: CM.CM_1.MONITOR                                              (addr=03000050 mask=ffffffff)  rw
    2: CM.CM_1.MONITOR.ACTIVE                                       (addr=03000050 mask=00000100)  r
    3: CM.CM_1.MONITOR.BAD_TRANS                                    (addr=03000052 mask=ffffffff)  rw
    4: CM.CM_1.MONITOR.BAD_TRANS.ADDR                               (addr=03000052 mask=000000ff)  r
    5: CM.CM_1.MONITOR.BAD_TRANS.DATA                               (addr=03000052 mask=00ffff00)  r
    6: CM.CM_1.MONITOR.BAD_TRANS.ERROR_MASK                         (addr=03000052 mask=ff000000)  r
    7: CM.CM_1.MONITOR.COUNT_16X_BAUD                               (addr=03000050 mask=000000ff)  rw
    8: CM.CM_1.MONITOR.ENABLE                                       (addr=03000050 mask=00010000)  rw
    9: CM.CM_1.MONITOR.ERRORS                                       (addr=03000054 mask=ffffffff)  rw
   10: CM.CM_1.MONITOR.ERRORS.CNT_AXI_BUSY_BYTE2                    (addr=03000055 mask=ffff0000)  r
   11: CM.CM_1.MONITOR.ERRORS.CNT_BAD_SOF                           (addr=03000055 mask=0000ffff)  r
   12: CM.CM_1.MONITOR.ERRORS.CNT_BYTE2_NOT_DATA                    (addr=03000056 mask=0000ffff)  r
   13: CM.CM_1.MONITOR.ERRORS.CNT_BYTE3_NOT_DATA                    (addr=03000056 mask=ffff0000)  r
   14: CM.CM_1.MONITOR.ERRORS.CNT_BYTE4_NOT_DATA                    (addr=03000057 mask=0000ffff)  r
   15: CM.CM_1.MONITOR.ERRORS.CNT_TIMEOUT                           (addr=03000057 mask=ffff0000)  r
   16: CM.CM_1.MONITOR.ERRORS.CNT_UNKNOWN                           (addr=03000058 mask=0000ffff)  r
   17: CM.CM_1.MONITOR.ERRORS.RESET                                 (addr=03000054 mask=00000001)  rw
   18: CM.CM_1.MONITOR.HISTORY                                      (addr=03000051 mask=ffffffff)  r
   19: CM.CM_1.MONITOR.HISTORY_VALID                                (addr=03000050 mask=0000f000)  r
   20: CM.CM_1.MONITOR.LAST_TRANS                                   (addr=03000053 mask=ffffffff)  rw
   21: CM.CM_1.MONITOR.LAST_TRANS.ADDR                              (addr=03000053 mask=000000ff)  r
   22: CM.CM_1.MONITOR.LAST_TRANS.DATA                              (addr=03000053 mask=00ffff00)  r
   23: CM.CM_1.MONITOR.LAST_TRANS.ERROR_MASK                        (addr=03000053 mask=ff000000)  r
   24: CM.CM_1.MONITOR.SM_TIMEOUT                                   (addr=0300005a mask=ffffffff)  rw
   25: CM.CM_1.MONITOR.UART_BYTES                                   (addr=03000059 mask=ffffffff)  r
```
To confirm the SM and CM are at the same BAUD rate, you can check COUNT_16X_BAUD, which sets the baud rate as follows
```
>nodes CM.CM_1.MONITOR.COUNT_16X_BAUD V
    1: CM.CM_1.MONITOR.COUNT_16X_BAUD                               (addr=03000050 mask=000000ff)  rw
       Baud 16x counter.  Set by 50Mhz/(baudrate(hz) * 16). Nominally 27d
```
LAST_TRANS gives you the decoded version of the last transaction (it basically stops when something bad happens, so sometimes not all of it is valid)

HISTORY holds the last 4 uart bytes send (HISTORY_VALID masks which of those bytes are valid).
