# SM Filesystem and Kernel Build

This section contains instructions to build and deploy the filesystem and kernel to Zynq on Apollo Service Module (SM).

## Setup

The repository for the filesystem and kernel source code can be found [here](https://github.com/apollo-lhc/SM_ZYNQ_FW). The filesystem is located under the directory `os`, and the kernel is located under the directory `kernel`. Clone the repository via `git` as usual to get started:

```bash
git clone https://github.com/apollo-lhc/SM_ZYNQ_FW.git
```

Using the source code in this repository, one can do the following:

* Build the firmware for the Zynq (**required** for the kernel build)
* Build the filesystem
* Build the Linux kernel

## Building Firmware

The firmware can be built in the root directory of the project. The configuration for each SM type (`rev1`, `rev2` etc.) are located under it's sub-directory under `configs` directory. The address table XML files are located under `address_table/modules`. 

As an example, to build the firmware for `rev2a_xczu7ev` SM, one can execute the following:

```bash
# Bring the sub-modules up to date
make init

# Do the firmware build
make rev2a_xczu7ev
```

## Building the Linux Kernel

Once the firmware build is completed, the Linux kernel can be built, using the source code under the `kernel` directory. Similar to the FW build, the build configuration for each SM type is defined under `configs` directory.

Build command is very similar to the FW build, for example for `rev2a_xczu7ev` SM:

```bash
cd kernel/
make rev2a_xczu7ev
```

### Patching the Kernel

Patches to kernel configuration can be specified under the following directory: `configs/<HW>/kernel/linux`. Here, under `linux-xlnx` directory, one can find the user specified `.cfg` files that has several patches to the kernel, and all these files **must** be added to the `SRC_URI` variable on the `linux-xlnx_%.bbappend` file. For more details, please see the documentation [here](https://docs.yoctoproject.org/3.1.19/) by Yocto.

### Deploying the Kernel

The output of the kernel build should be a tarball file, named after the SM revision, `<SM revision>.tar.gz`. To deploy this kernel build to a SM, one can do the following:

* Find out the mount point of the first partition, `/dev/mmcblk0p1`. This is typically `/fw/SM` in most recent images, and `/fw` for older images. Please execute `mount | grep mmcblk` to find out the mount point of the first partition.
* Copy the tarball under the location of the first partition via `scp`.
* Untar the tarball: `tar -p -zxf <SM_revision>.tar.gz`.
* Reboot the SM: `sudo reboot`

## Building the Filesystem

The source code for filesystem build is located under the `soc-os` git repository, checkout the the latest tag:
```
git clone https://gitlab.com/apollo-lhc/soc-os
git checkout v1.2.0
```

The recommended build is using a Docker container, as explained below. It can also be built without docker, but that requires sudo access in the machine you run.

**Note:** Please make sure that you have the `secure` files (you can request from Dan Gastler) before you start the build. These are the files with user/password information, and are not available in public. The build will fail without these files.

To execute the build for a specific SM revision, you can execute `make` as follows. To continue the example on `rev2a_xczu7ev`:

```bash
cd os/

# Do the docker-based build
make docker_rev2a_xczu7ev
```

### Deploying the Filesystem

Identical to the kernel build, the user should get a tarball file as the build output, named after the SM revision: `<SM_revision>.tar.gz`. To deploy the build, one can do the following:

* Copy the tarball to the mount point of second partition, `/dev/mmcblk0p2`, which is typically the root directory, `/`.
* Untar it: `tar --numeric-owner -p -zxf <SM_revision>.tar.gz`
* Reboot the SM: `sudo reboot`

## Boot Types For the SM

When rebooting, the boot type of Zynq can be configured from the IPMC terminal. Following boot types are recognized:

* SDCard (`bootmode 1` on IPMC)
* EMMC (`bootmode 0` on IPMC)
* Netboot
