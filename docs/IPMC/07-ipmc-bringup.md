# IPMC Bring-Up Instructions

This section describes how to configure a new IPMC to be fully working with the rest of the Apollo SM. 

## Necessary Configurations

The user typically needs to configure the following via performing writes to the EEPROM:

* The EEPROM revision number
* The ID number and the revision of the SM on EEPROM
* The desired boot mode for the SM (from microSD card or EMMC)
* Which SD card to boot from (in case booting from microSD card)
* MAC address for the ETH0 and ETH1 ports of the SM 

All these configurations will be explained in more detail below.

These configurations can be done using the `telnet` interface of the IPMC. If the IP address of the IPMC is known, the user can simply launch the `telnet` interface as follows:

```
# Connect to the telnet interface of the IPMC
telnet 192.168.X.Y
```

The user should see the command prompt `>>`, after pressing `Enter` a couple of times. To exit this interface, first press `Ctrl` + `]`, and then in the `telnet` prompt, enter "quit" (or "q" for short).

In case the IP address of the IPMC is not known, a good website to check is the "Shelf Status" webpage, which can be found [here](https://ohm.bu.edu/~apollo/shelf_status.html).

Below, you can find the detailed explanation of the necessary configurations, and the CLI commands to execute.

### EEPROM Revision

EEPROM revision is set via `verwr` command. For the most recent EEPROM version which includes the MAC address checksums for SM, user should choose EEPROM revision 1:

```
>> verwr 1
```

### SM ID Number / Revision

The ID number of the SM should be set via the `idwr` command. As an example, if the SM is labeled as `211`, the command would be:

```
>> idwr 211
```

Furthermore, to set the SM revision (e.g. `rev2`), please use the `revwr` command. For example, for `rev2a` SMs, the command would be:

```
>>> revwr 2
```

**Note:** Please note that since the revision data must be numeric, `rev2a` SMs are recorded as `rev2` in the EEPROM.

### Boot Mode

The boot mode indicates where the filesystem and boot files should be read from. A boot mode of `0` indicates booting from EMMC (on rev2a SMs), while `bootmode=3` indicates booting from the microSD card. The boot mode can be set in the EEPROM via the `bootmode` command. In case of booting from the SD card, to pick the correct SD card slot (on the back of the SM), the user should also specify `sdsel 0`:

```
# Configuration if we want to boot from the SD card 
# located at the back of the SM

>> bootmode 3
>> sdsel 0
```

### MAC Address for the SM

`ethmacwr` command is used to set the desired MAC address of the SM for ETH0 and ETH1 ports. Please see the instructions [here](https://apollo-lhc.gitlab.io/Service-Module/zynq-eth-interface/) for details on how to set the MAC addresses using the IPMC terminal interface.

### Restarting the SM

Once all the configurations are set, the user can execute `eepromrd` to read the latest configuration stored in the EEPROM. If all checks out, the user can restart the SM via the `restart` command.

```
# Read the EEPROM to check the configuration
>> eepromrd

# Restart the SM (may disconnect terminal for rev1s)
>> restart
```

If the terminal is disconnected in the restarting process, exit the interface by first pressing `Ctrl` + `]`, and then entering "q" or "quit" in the `telnet` command prompt.

## Firmware Update

If new firmware (FW) is to be loaded on an OpenIPMC, the user can consult the FW upgrade instructions using `ipmitool` [here](https://apollo-lhc.gitlab.io/IPMC/05-ipmc-firmware-upgrade/#remote-programming-through-ipmi-tool). It is also possible to connect a debugger to the Apollo SM, and write new FW to the IPMC flash memory. For the instructions for those, please check [here](https://apollo-lhc.gitlab.io/IPMC/06-ipmc-debugging/).

## Bootloader Installation

**Note:** This section assumes that you have a STM32 debugger device connected to your Apollo blade. If that is not the case, please first check out the instructions [here](https://apollo-lhc.gitlab.io/IPMC/06-ipmc-debugging/) to set up the debugger device.

In this section, we'll go over two topics:

* How to figure out if a bootloader is already installed on an OpenIPMC
* If not installed, how to build and install a bootloader using the STM32CubeIDE and STM32CubeProgrammer interfaces

### How to figure out if a bootloader is installed

One can check if a bootloader is installed in an OpenIPMC in two ways:

* Connect to telnet CLI, and execute the `bootloader` command. The output will indicate whether a bootloader is installed (and which version, v0.1.0 is typically the standard).
* Execute an `ipmitool hpm check` command with the IPMB address of the IPMC, like the following:

```bash
ipmitool -H <Shelf Manager IP> -P "" -t <IPMB adr> hpm check
```

This command will print out all FW installed on the OpenIPMC. If a bootloader is installed, you should be able to see it in the output. An example output is given below:

```
--------------------------------------------------------------------------
|ID  | Name        |                     Versions                        |
|    |             |     Active      |     Backup      |      Deferred   |
--------------------------------------------------------------------------
|   0|Bootloader   |   0.10 E255A953 | ---.-- -------- | ---.-- -------- |
|*  1|CM7_fw       |   1.60 CA80139E | ---.-- -------- | ---.-- -------- |
|*  2|CM4_fw       |   0.00 00000000 | ---.-- -------- | ---.-- -------- |
--------------------------------------------------------------------------
(*) Component requires Payload Cold Reset
```

If you see the bootloader installed, you're all set! If not, please check out the instructions in the following section.

### How to install a bootloader

The bootloader repository can be found [here](https://gitlab.com/openipmc/openipmc-fw-bootloader). Please check out the `master` branch:

```bash
git clone https://gitlab.com/openipmc/openipmc-fw-bootloader.git
```

Once cloned, you can add the codebase as a project to the STM32CubeIDE and build the project (for CM7) there. The instructions for this are below.

1. In the STM32CubeIDE interface, go to File $\rightarrow$ Import.
2. In Import menu, go to General $\rightarrow$ Existing Projects into Workspace 
3. In Select root directory box, enter (or browse) the workspace directory path for the bootloader.
4. In Projects box, mark the `openipmc-fw-bootloader` project, and the nested projects for CM4 and CM7.
5. Press Finish. Once the import is complete, you should see a project architecture like the following:

<img src="../../images/ipmc_bootloader/project_explorer.png" width="350">

Now, we need to build the bootloader projects for CM4 and CM7 cores. On both `openipmc-fw-bootloader_CM4` and `openipmc-fw-bootloader_CM7` projects, do the following:

* Right click on the project
* Click on "Build Project"

Once the build is complete, you should see a binary file called `openipmc-fw-bootloader_CM7.bin` 
under the directory `CM7/Debug`. We will load this binary file to the OpenIPMC using the STM32 Programmer, the steps for this are explained below.

* Open the STM32CubeProgrammer interface. If the STM32 debugger device is connected to the Apollo blade, click "Connect" on the top right.
* On the left-hand side menu, click on the second icon to open "Erasing & Programming" menu.
* Set the file path to the binary file just built, and set the start address to `0x081E0000`, i.e. start of the flash sector 15. The configuration should look like this:

<img src="../../images/ipmc_bootloader/programming.png" width=750 />

* Click "Start Programming" to load the bootloader binary to flash sector 15.
* Once the download is finished, click on the fourth icon in the left-hand side menu, "MCU core". The page should look like this:

<img src="../../images/ipmc_bootloader/hardware_reset.png" width=750 />

* Here on this page, click "Hardware Reset" button on the right.
* The IPMC will boot back up, and after the reboot, please check if the bootloader is now installed using any of the two methods described above. You're all set!