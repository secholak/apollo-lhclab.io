# IPMC Inventory

The list of IPMCs that are registered can be viewed using the new [SnipeIT](https://snipeitapp.com/) inventory system, used by BU. It is also possible to create new IPMCs, and check them out to different Apollo Service Module instances within the same database.

List of all IPMCs can be found [here](http://ohm.bu.edu:8000/models/3). Note that in order to view this page, you need to login to the inventory system. This can be done in two ways:

* **Login as the read-only user:** For this, the username is guest, and the password is guestview, as also can be seen in the login page. You can view the list of IPMCs with this user, but you won't be allowed to edit any asset, or add a new one.

* **Request an account:** Request a user account from Dan Gastler, and use that for login. A regular user has permissions to update asset records, and/or add a new asset to the system.