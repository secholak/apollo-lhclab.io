# IPMC FW Loading via STM32

IPMC firmware (FW) can be ran through a debugger using an ST-LINK programmer and a STM32CubeIDE setup. Using this IDE, the FW execution can be stopped through user-defined breakpoints, and variables can be monitored during the program execution, allowing potential bugs in the code to be understood. This section aims to describe the workflow for the debugging.

## Setting Up The Connector

First, the ST-LINK programmer must be properly connected to the Apollo Service Module (SM). Since the ST-LINK debugger device won't automatically connect to the SM connector, one needs to use an adapter board. The ST-LINK debugger should be connected to this adapter as shown in the image below. The adapter has spots for `rev1` and `rev2` connections. As an example, one can see how the adapter should be used to connect to the `rev2a` SM in the image below.

### Debugger Connection to Rev2a SM:

<img src="../../images/ipmc_debugger_rev2a.png" title="IPMC Debugger Setup on Rev2a SM" width="350" />

**Note:** Please note how the pin #1 (marked by the black line on the connecting wires) is located on the right-side of the `rev2` adapter, and the left side of the `rev2a` SM connector.

For further details about the hardware spcifications for the adapter and the SM, one can check the following KiCad schematic files:

* Rev1 & Rev2 SM adapter schematics [here](https://github.com/andrewpeck/ipmc_jtag/blob/master/STM_to_Apollo_v2_20210329.zip)
* Apollo SM HW schematics [here](https://gitlab.com/apollo-lhc/hardware/apollosm-hw) (check out the `rev*-as-built` tags for the SM revision of interest)

## STM32 CubeIDE Setup

Once the ST-LINK debugger device is connected to the SM, next step is to clone the OpenIPMC FW repository, and then configure the IDE workspace.

**Prerequisites:**

* If at BU, you can execute the following instructions on the `server-room` machine, so please first SSH to that machine with your account. If you are
connecting from a public network, please note that you first need to SSH to `tesla`, using it as a gateway to `server-room`. You can see [here](https://apollo-lhc.gitlab.io/Working-At-BU/Getting-Started/#computer-accounts) for instructions on this.
* In general, this should work with any device where the STM32CubeIDE software is installed.

### Configure the Workspace

Below are the instructions to set up the IDE workspace.

* Clone the Apollo-based OpenIPMC FW ([here](https://gitlab.com/BU-EDF/openipmc-fw)), and the core OpenIPMC FW ([here](https://gitlab.com/openipmc/openipmc-fw/)) as a submodule, do a recursive clone as follows:

```bash
# Clone the repository together with submodules
git clone --recursive https://gitlab.com/BU-EDF/openipmc-fw.git
```

* Open the STM32CubeIDE from the host device. At BU, this is typically the `server-room` machine.

* In STM32CubeIDE, go to *File* &rarr; *Import*.

* In the pop-up wizard, pick *General* &rarr; *Existing Projects into Workspace*, as can be seen below.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/import_wizard_view.png" width=400 >
</div>

* In *Select root directory:* box, enter (or browse) the path to the `openipmc-fw` directory, where the entire FW project was cloned. Please note that this must be the
**root directory of the project.** When prompted which projects to import, pick *openipmc-fw*, *openipmc-fw_CM4* and *openipmc-fw_CM7* projects for import.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/project_import_view.png" width=400 >
</div>

* Click *Finish*. After this, on the *Project Explorer* tab on the left-hand side, you should be able to see all projects imported.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/project_explorer_view.png" width=400 >
</div>

### Build the Project

To compile the firmware for CM4 and CM7 cores, do the following:

* On the *Project Explorer* tab, right click on the *openipmc-fw_CM4* project, and click on *Build Project*.
* Repeat for *openipmc-fw_CM7* project.

### Build Settings & Troubleshooting

In case of errors, you can access the build configuration of each project by right-clicking to the project name, and clicking on *Properties*.
Following checks can be useful to debug build problems:

* Check if all the include paths you want are there. Under the *Properties* menu, you can go to *C/C++ Build* &rarr; *Settings* &rarr; *MCU GCC Compiler* &rarr; *Include paths*.
Here, you can add or edit include paths. When you are finished, click on *Apply and Close*.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/include_paths_config.png" width=400 >
</div>

* Check for source code search paths. Under the *Properties* menu, you can go to *C/C++ General* &rarr; *Paths And Symbols*. Then on the top menu, click on *Source Location*.
Here, you can add or edit paths where the compiler and linker will search for C source code. When you are finished, click on *Apply and Close*.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/source_locs.png" width=400 >
</div>

* You can also check the optimization level that the compiler uses from *C/C++ Build* &rarr; *Settings* &rarr; *MCU GCC Compiler* &rarr; *Optimization*. The default configuration
currently is to remove all optimizations, hence the `-O0` flag. However, please note that the FW builds over GitLab CI/CD uses size optimization, `-Os`. To update the optimization
level while compiling on the IDE, change the value in the *Optimization level* box, and click *Apply and Close*.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/opt_level_config.png" width=400 >
</div>

### Launch a Debugging Session

To load the OpenIPMC firmware to CM4 or CM7 core and launch a debugging session, 
right click on the respective project and click *Debug As* &rarr; *1 STM32 C/C++ Application*. A pop-up window should prompt you to select the binary
to load into the OpenIPMC, select *openipmc-fw_CM7.elf* (or the CM4 one, if you are loading that) and click *OK*.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/binary_selection.png" width=250 >
</div>

If this is the first time you are running the FW,
you will be prompted with a configuration screen. There, under the *Debugger* tab, make sure that the interface is set to SWD, and the *ST-LINK S/N* box
is checked out. That box should have the serial number of the connected STM32 debugger device, to obtain that, click on *Scan*.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/debug_config.png" width=500 >
</div>

Once this is all done, click *Apply* and exit the window. Note that you can also have access to this menu by right-clicking the project name and clicking on
*Debug As* &rarr; *Debug Configurations*.

After the configuration is done, you can right-click on the project and then click on *Debug As* &rarr; *1 STM32 C/C++ Application*, and select the `.elf` binary
corresponding to the OpenIPMC FW to load as shown above. Given that the FW build finishes OK and the STM32 debugger device is connected to the Apollo blade, this will
launch a debugging session.

By default, the debugger session will stop at the beginning of the `main` function call. To resume the execution, click on the *Resume* button at the top menu, the left-most
button in the figure below. To pause the execution at any time, click on the *Pause* button (middle), and click on *Stop* (right) to exit out of the debugging session.

<div style="text-align: center;">
    <img src="../../images/stm32_workspace_setup/debugger_buttons.png" width=100 >
    <figcaption>In order from left to right: Resume, pause and stop buttons on the IDE (in the top menu).</figcaption>
</div>

### Load FW Without Launching a Debugging Session

To just load new firmware to OpenIPMC via STM32CubeIDE, without launching a debugging session, you can:

* Right-click the project name, and select *Run As* &rarr; *1 STM32 C/C++ Application*.
* In the pop-up window, pick the `.elf` binary file corresponding to the OpenIPMC FW, same as above, and click *OK*.

This should load new firmware to the flash memory of OpenIPMC and exit. If you have not configured the debugging configuration yet, you will first be prompted with a configuration
menu, where you can select the interface type an enter the serial number of the debugger device. Please see the above section for instructions on this.