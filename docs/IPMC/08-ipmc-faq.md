# IPMC FAQ

This section aims to answer some of the most frequently asked questions about working with IPMCs.

## How Do I Retrieve The IP Address Of IPMC?

Most of the IPMCs will be assigned IP addresses in the `192.168.22` subnet. In the most recent FW versions, the typical IP address will be `192.168.22.X`, where `X` is the serial number of the OpenIPMC. 

There are several alternative ways of retrieving the IP address of an IPMC, and those will be outlined below.

### Using Zynq I2C Registers

If the SM is up, one can `ssh` to the SM and use `BUTool` CLI to read the IPMC IP address from I2C registers. As an example, here is how one can read the IPMC IP from `apollo209-1`:

```
# Log in to the SM
$ ssh cms@apollo209-1

# Launch BUTool on SM209
[cms@apollo209-1 ~]$ BUTool.exe -a

>status 1 IPMC
Process done
             IPMC| ADDR|  ID|            IP|
               --|-----|----|--------------|
              ETH|     |    | 192.168.22.37|
               RN|     |   1|              |
             SLOT| 0x86|    |              |
               SN|     | 209|              |

SW VER: -1
```
The IPMC IP address is displayed under the `IP` column.

### Using IPMC Sensors

**Note:** This method **only** works for IPMCs which have FW version >= `v1.4.0`.

If the SM is not booted, but `ipmitool` commands are working, one can execute the `sensor` command from `ipmitool` to get the reading from all available sensors. In supporting IPMCs, the last 4 sensors will return the IP address of the IPMC (1-byte each). As an example, to read the IPMC IP address from the IPMC at slot `0x86` at Schroff shelf, one can do:

```
$ ipmitool -H 192.168.10.172 -P "" -t 0x86 sensor
...
IPMC IP Byte 0   | 192.000    | unspecified | ok    | na        | na        | na        | na        | na        | na        
IPMC IP Byte 1   | 168.000    | unspecified | ok    | na        | na        | na        | na        | na        | na        
IPMC IP Byte 2   | 22.000     | unspecified | ok    | na        | na        | na        | na        | na        | na        
IPMC IP Byte 3   | 37.000     | unspecified | ok    | na        | na        | na        | na        | na        | na 
```

The user can read out the IPMC IP address from these sensors.

## How Do I Retrieve The IPMB (Slot) Address?

The slot address can be either read from I2C registers of Zynq, or the IPMC's `telnet` interface. Knowing the IPMB address is useful to execute `ipmitool` commands for this IPMC.

### Using Zynq I2C Registers

Please refer to the above section [here](https://apollo-lhc.gitlab.io/IPMC/08-ipmc-faq/#using-zynq-i2c-registers) for the instructions on retrieving `IPMC` table from I2C registers. The slot address will be printed on `SLOT` row and `ADDR` column.

### Using Telnet CLI

If the IP address of the IPMC is known, the user can launch the `telnet` CLI, and execute the `info` command to get the slot address. For example:

```
telnet 192.168.22.37

>> info

OpenIPMC-HW
Firmware commit: 3932ba5c

Target Board: APOLLO-OPENIPMC
IPMB-0 Addr: 0x86
...
```

The IPMB (slot) address is displayed as `0x86` in the command output.

## How Do I Retrieve IPMC FW Version?

The FW commit hash can be retrieved from multiple places, either from running an `ipmitool fru` command to the IPMC, or running `info` on the `telnet` CLI.

### Using Telnet CLI

Please refer to the instructions [here](https://apollo-lhc.gitlab.io/IPMC/08-ipmc-faq/#using-telnet-cli) to open a `telnet` CLI session, and execute `info` command. In the command output, the firmware commit will be displayed.

### Using IPMITool

If the IPMB (slot) address of the IPMC is known, one can execute an `fru` command to retrieve the FW hash. For example, to get the FW hash for IPMC on slot `0x86` of the Schroff shelf, one can execute:

```
$ ipmitool -H 192.168.10.172 -P "" -t 0x86 fru
FRU Device Description : Builtin FRU Device (ID 0)
 Board Mfg Date        : Thu Dec  1 16:38:00 2022
 ...
 Product Asset Tag     : 3932BA5C
```

The FW hash will be displayed on the field named as "Product Asset Tag".

## How Do I Update IPMC FW?

This can be done remotely through `ipmitool`, or via a debugger connection. For `ipmitool` instructions, please see [here](https://apollo-lhc.gitlab.io/IPMC/05-ipmc-firmware-upgrade/#remote-programming-through-ipmi-tool). For debugger instructions, please see [here](https://apollo-lhc.gitlab.io/IPMC/06-ipmc-debugging/#other-configurations).  

## How Do I Roll Back to Backup FW?

The OpenIPMC stores one current (running) and one backup FW, and it is possible to use `ipmitool` to retrieve information about both, and roll back to the backup FW being stored.

To get information about the current and backup FW, one can execute the `hpm` commands shown below. If the OpenIPMC is located on the slot `0x86`, with shelf manager IP `192.168.10.172`, the commands are the following: 

```
# Get information about the running FW
$ ipmitool -H 192.168.10.172 -P "" -t 0x86 hpm compprop 1 1

# Get information about the backup FW
$ ipmitool -H 192.168.10.172 -P "" -t 0x86 hpm compprop 1 3
```

To actually perform the roll back to the backup FW, one can execute:
```
$ ipmitool -H 192.168.10.172 -P "" -t 0x86 hpm rollback
```

## Where Is The IPMC FW Repo?

The Apollo-customized OpenIPMC FW repository is hosted on GitLab under `BU-EDF` account, and can be found in [this](https://gitlab.com/BU-EDF/openipmc-fw/-/tree/master) link.

## How To Install A Bootloader?

If the bootloader is not present in the OpenIPMC flash 15 (i.e. addr `0x081E0000`), the `hpm activate` command will not work, hence a remote FW upgrade will fail. The instructions to figure out if there is a bootloader installed, and how to install one can be found in [this](https://apollo-lhc.gitlab.io/IPMC/07-ipmc-bringup/#bootloader-installation) section under "IPMC Bringup". 

## How Can I Update Sensor Thresholds via IPMITool?

Starting from OpenIPMC FW [v1.7.0](https://gitlab.com/BU-EDF/openipmc-fw/-/tags/v1.7.0), sensor thresholds can be dynamically adjusted using `ipmitool` commands.

```
# List all available sensors
ipmitool -H <Shelf Manager IP> -P "" -t <IPMB Address> sensor

# Update a threshold type (see below for threshold definitions) for a given sensor
ipmitool -H <Shelf Manager IP> -P "" -t <IPMB Address> sensor thresh <Sensor Name> <Threshold Type> <Threshold Value>
```

Supported threshold types are (in order of increasing severity):

* **Upper non-critical:** unc
* **Upper critical:** ucr
* **Upper non-recoverable:** unr

As an example, if a sensor is called "Zynq Temperature", we can update the upper non-recoverable threshold to 85 by using:

```
ipmitool -H <Shelf Manager IP> -P "" -t <IPMB Address> sensor thresh "Zynq Temperature" unr 85
```