# IPMI Resets

**Note:** These reset features are not yet integrated into the stable firmware of the OpenIPMC. They are projected to be a part of the next FW release, *v1.8.0*.

## Resets on Apollo Service Module

OpenIPMC implements three types of resets on the Apollo Service Module:

* Cold reset
* Warm reset
* Graceful reboot

These resets and how to trigger them using `ipmitool` commands are described below.

### Cold Reset

This reset directly cuts off 12V power on the Zynq and launchs the power-up sequence for the Apollo Service Module afterwards. It can be initiated with:

```
ipmitool -H <Shelf Manager IP> -P "" -t <Slot Address> picmg frucontrol 0 0
```

### Warm Reset

This reset initiates the shutdown procedure in Zynq and waits for 30 seconds for a graceful shutdown. After the Zynq has shut down, the 12V power is disabled
and shortly after, power-up sequence is launched. If Zynq fails to shutdown in 30 seconds, the 12V power will be cut off anyway.

Warm reset can be initiated with:

```
ipmitool -H <Shelf Manager IP> -P "" -t <Slot Address> picmg frucontrol 0 1
```

### Graceful Reboot

This reset is identical to warm reset, except the timeout for the Zynq shutdown is much longer, 5 minutes. It can be initiated with:

```
ipmitool -H <Shelf Manager IP> -P "" -t <Slot Address> picmg frucontrol 0 2
```

**Note:** In the above `picmg frucontrol` commands, the first `0` represents the FRU ID of the OpenIPMC. In the current implementation,
since there is one OpenIPMC per ATCA slot, a default ID of `0` is used. Trying to invoke `picmg frucontrol` with a non-zero FRU ID will
fail.

## Resets on IPMC

Using `ipmitool` commands, the IPMC itself can also be reset. Currently, cold reset of the OpenIPMC is implemented. This can be invoked via:

```
ipmitool -H <Shelf Manager IP> -P "" -t <Slot Address> mc reset cold
```
