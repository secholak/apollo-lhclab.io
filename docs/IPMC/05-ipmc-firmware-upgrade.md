# IPMC Firmware Build / Upgrade

## Building the Firmware

### GitLab CI/CD (Recommended)

OpenIPMC firmware for Apollo boards (repository [here](https://gitlab.com/BU-EDF/openipmc-fw/-/tree/master)) can be built automatically
using GitLab CI/CD jobs. These jobs will be triggered whenever a commit is pushed to the remote repository. The configuration (e.g., the set
of commands being run for each job) is stored in the `.gitlab-ci.yml` file, located in the top-level directory of the Apollo-based OpenIPMC FW repository.

The resulting binary and HPM files can be downloaded from the job artifacts. To do this, you can follow these steps:

* Go to the BU-EDF OpenIPMC FW repository [here](https://gitlab.com/BU-EDF/openipmc-fw/-/tree/master).
* On the left-hand side menu, click on *Build* &rarr; *Pipelines*.
* For the build job you want to load firmware for, click on *Download artifacts* button on the right.

There are two artifacts per job you can download:

* `build-job:archive` contains all the firmware files.
* `build-job:dotenv` contains a log file with build information.

<div style="text-align: center;">
    <img src="../../images/ipmc_fw/gitlab_ci_fw_load.png" width=1000 >
</div>

This will download a `.zip` file containing several `.bin` and `.hpm` files with CM4, CM7 firmware and the bootloader. This can be unzipped with `unzip` via the
command line, or some other method you prefer. 

Binary files can be used to upload firmware to the OpenIPMC
using a microUSB or STM32 debugger connection, 
and HPM files can be used for remote upload using `ipmitool` (see below).

### Interactively on Terminal

To interactively build the OpenIPMC firmware, just clone the repository and its submodules, and build using `make`, as can be seen below.

**Note:** Please note that `arm-none-eabi-gcc` **must** be installed on your system for this build to work. For dependency reasons such as this one, 
typically FW build over GitLab CI/CD jobs (explained above) is preferred.

```bash
git clone --recursive https://gitlab.com/BU-EDF/openipmc-fw.git
cd openipmc-fw

# Make sure all the submdoules are pointing to the correct version
make init

# Build binaries and HPM files
make -j4 hpm
```

## Loading The Firmware

### Remotely Through IPMI Tool

OpenIPMC FW can be loaded remotely to Cortex-M4 and Cortex-M7 processor cores using `ipmitool` commands, using `hpm` upgrade files. 
These files can be loaded from GitLab CI/CD job artifacts (preferred) or
built manually (see above). Given an `hpm` file, you can execute the following to remotely update the firmware and restart the OpenIPMC:

```
# Load the firmware into a temporary memory location in the OpenIPMC microcontroller.
ipmitool -H <shelf IP> -P "" -t <slot address> hpm upgrade <firwmare file>

# Restart the OpenIPMC with the newly loaded firmware.
ipmitool -H <shelf IP> -P "" -t <slot address> hpm activate
```

The `<shelf IP>` field should be substituted by the actual IP address of the shelf manager. For shelves at BU, the following apply:

- `192.168.10.172` is the Schroff shelf
- `192.168.10.171` is the Comtel shelf

The slot address (given by `-t`) refers to the IPMB address of the target OpenIPMC. [This](https://apollo-lhc.gitlab.io/Quick-Start-Guide/04-Cheatsheets/#slot-numerology)
webpage has a nice table of possible IPMB addresses and the corresponding physical slot numbers.

For Apollo hardware at Cornell and TIF, information such as the shelf manager IP addresses and the IPMB addresses can be found in 
[this](https://twiki.cern.ch/twiki/bin/view/CMS/Ph2TrackerTIF) TWiki page.

It is possible to:

* Upload firmware for Cortex-M4 and Cortex-M7 cores separately
* Upload firmware for both cores at the same time using a single `hpm` file

Both options are described in the below sections.

#### Separately for CM4 and CM7 cores

From the GitLab CI/CD jobs, you can obtain separate `hpm` files for CM4 and CM7 cores:

* One for Cortex-M4 firmware: `upgrade_CM4.hpm`
* One for Cortex-M7 firmware: `upgrade_CM7.hpm`

These HPM files can be used separately to upload firmware for Cortex-M4 and M7 cores. As an example, 
to upgrade firmware of the OpenIPMC at address 0x8e on the Schroff shelf, for both cores, you would run:

```
# Update Cortex-M7 firmware
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm upgrade upgrade_CM7.hpm
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm activate

# Update Cortex-M4 firmware
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm upgrade upgrade_CM4.hpm
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm activate
```

In cases where the upgrade step is skipped (for example, this can happen when the commit hashes of the current FW file and the upgrade file are the same), the `force` option can be used to force the upgrade:

```
# Force the upgrade step
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm upgrade upgrade_CM7.hpm force

# Activate the new FW as before
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm activate
```

At any time, the FW loaded on an OpenIPMC (both for CM4 and CM7 cores) and the bootloader version can be checked:

```
# Check the CM4/CM7 FW versions and the bootloader
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm check
```

**Note:** Please note that the `hpm activate` command, which should reset the OpenIPMC with the newly loaded firmware, 
will not succeed if a bootloader is not installed on an OpenIPMC. Please see [here](https://apollo-lhc.gitlab.io/IPMC/07-ipmc-bringup/#bootloader-installation)
for instructions to:

* Check if a bootloader is installed in an OpenIPMC.
* If not, how to install one.

It is also possible to roll back to the backup FW stored on the OpenIPMC, using `ipmitool hpm` commands. For instructions on this, please see the relevant section on the IPMC FAQ page [here](https://apollo-lhc.gitlab.io/IPMC/08-ipmc-faq/).

#### Updating CM4 and CM7 at the same time

To update the firmware in Cortex-M4 and M7 cores at the same time, you can use the `upgrade_FULL.hpm` file from the GitLab CI/CD job artifacts. The command for uploading firmware
and restarting the OpenIPMC is identical to before:

```
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm upgrade upgrade_FULL.hpm
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm activate
```

As before, you can check the CM4 and CM7 firmware versions and the bootloader version:

```
ipmitool -H 192.168.10.172 -P "" -t 0x8e hpm check
```

### Through the STLINK Programmer Device

If an STM32 debugger device is attached to the OpenIPMC, firmware can also be loaded via that connection. If the resulting binary file (for Cortex-M7 firmware)
is placed in `./openipmc-fw/CM7/openipmc-fw_CM7.bin`, and you have `st-flash` command line tool installed in your system, you can execute:

```
make load_st
```

Please see [here](https://apollo-lhc.gitlab.io/IPMC/06-ipmc-debugging/#setting-up-the-connector) for instructions to set up the STM32 debugger connection.
In the same page, you can also find instructions on how to build and load OpenIPMC firmware using the STM32 Cube IDE.

### Through a MicroUSB Connection

**Note:** This method will **not** work when the OpenIPMC is attached to an Apollo board. Please use an eval board for this type of firmware loading.

Firmware to the OpenIPMC can also be loaded by attaching a USB cable to its micro-USB B port. To trigger the USB bootloader, you need to do the following:

* Press and hold the BOOT0 button.
* With BOOT0 pressed, press and release RST button.
* Finally, release the BOOT0 button.

As you are doing the above steps, run `sudo dmesg -w` on the computer which is connected to the other end of the USB cable, and watch for the expected output below:

```
[1031271.857760] usb 1-2.2: USB disconnect, device number 49
[1031272.591876] usb 1-2.2: new full-speed USB device number 50 using ehci-pci
[1031272.702883] usb 1-2.2: New USB device found, idVendor=0483, idProduct=df11, bcdDevice= 2.00
[1031272.702886] usb 1-2.2: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[1031272.702888] usb 1-2.2: Product: DFU in FS Mode
[1031272.702890] usb 1-2.2: Manufacturer: STMicroelectronics
[1031272.702891] usb 1-2.2: SerialNumber: 200364500000
```

The location of each button can be found in the OpenIPMC schematic below. BOOT0 button is on the top-left of the bottom image (labeled as "MCU reset"), and the RST
button is on the top image, towards the top-right (slightly above the SPI IO expanders). The micro-USB B port can also be seen on the top image, located on the top left.

<img src="../../images/ipmc_schematic/ipmc_schematic.png" width=1000 />

<div style="text-align: center;">
    <img src="../../images/ipmc_fw/ipmc_fw_load_via_usb.png" width=500 />
</div>

Once the steps above are done, the OpenIPMC should present itself as a DFU device, which can be connected to using the `dfu-util` command line tool. To check if `dfu-util`
can recognize the OpenIPMC now, do the following:

```bash
# Running dfu-util to see if the OpenIPMC device is visible
dfu-util --list

dfu-util 0.11-dev

Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
Copyright 2010-2021 Tormod Volden and Stefan Schmidt
This program is Free Software and has ABSOLUTELY NO WARRANTY
Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

Found DFU: [0483:df11] ver=0200, devnum=50, cfg=1, intf=0, path="1-2.2", alt=0, name="@Internal Flash /0x08000000/16*128Kg", serial="200364500000"
Found DFU: [0483:df11] ver=0200, devnum=50, cfg=1, intf=0, path="1-2.2", alt=1, name="@Option Bytes /0x5200201C/01*128 e", serial="200364500000"
```

If you can locate the lines starting with *Found DFU* above, your device can recognize the OpenIPMC and you are ready to load firmware into the flash
using `dfu-util`. You can install Cortex-M7 firmware to address `0x08000000` on the OpenIPMC flash. 
Given the `openipmc-fw_CM7.bin` binary file from the firmware build (see above on
how to build/retrieve it), you can use the following command to load the firmware:

```bash
sudo /usr/local/bin/dfu-util -s 0x08000000 -d 0483:df11 -a 0 -D ./openipmc-fw_CM7.bin
```

If all good, you should be seeing an output that looks like the following:

```
Opening DFU capable USB device...
Device ID 0483:df11
Device DFU version 011a
Claiming USB DFU Interface...
Setting Alternate Interface #0 ...
Determining device status...
DFU state(2) = dfuIDLE, status(0) = No error condition is present
DFU mode device DFU version 011a
Device returned transfer size 1024
DfuSe interface name: "Internal Flash   "
Downloading element to address = 0x08000000, size = 243648
Erase   	[=========================] 100%       243648 bytes
Erase    done.
Download	[=========================] 100%       243648 bytes
Download done.
File downloaded successfully
```

To check if the firmware is properly loaded, you can use `ipmitool` commannds to retrieve the FW version when the OpenIPMC is plugged into an Apollo in a shelf.
You can either use `hpm check` as explained above, or use `fru` (see [here](https://apollo-lhc.gitlab.io/IPMC/08-ipmc-faq/#using-ipmitool)) to retrieve the firmware
commit hash.