# FW updating

A new FW file can be loaded by the root user using the apollo-updater python script on the blade.
By convention, download the fw tar.gz file from above to the /root/fw directory.
Then the FW can be updated with the command

```
[root@apollo211-1 ~]# /opt/apollo-updater/apollo-updater.py --fw /root/fw/name.tar.gz
```

This will take a few minutes and then re-boot the blade using the reboot command.

Ex:
```
[root@apollo211-1 fw]# wget https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/jobs/5323035148/artifacts/raw/kernel/zynq_build/rev2a_xczu7ev.emmc.docker/rev2a_xczu7ev.tar.gz
--2023-10-19 00:55:57--  https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/jobs/5323035148/artifacts/raw/kernel/zynq_build/rev2a_xczu7ev.emmc.docker/rev2a_xczu7ev.tar.gz
Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 11001351 (10M) [application/octet-stream]
Saving to: ‘rev2a_xczu7ev.tar.gz’

rev2a_xczu7ev.tar.gz      100%[=====================================>]  10.49M  48.0MB/s    in 0.2s    

2023-10-19 00:55:58 (48.0 MB/s) - ‘rev2a_xczu7ev.tar.gz’ saved [11001351/11001351]

[root@apollo211-1 fw]# /opt/apollo-updater/apollo-updater.py --fw ./rev2a_xczu7ev.tar.gz
> Extracting input file     : ./rev2a_xczu7ev.tar.gz
> Output directory to save  : /fw/SM
> Extracting ./rev2a_xczu7ev.tar.gz
> Extraction complete!
> Done!
[root@apollo211-1 fw]# sync
[root@apollo211-1 fw]# reboot
```



