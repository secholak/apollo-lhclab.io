# Apollo Memory structure
#### Local Interconnect
| Block  | USP base address | 7s base address | size |
| ---    | ---      | ---     | ---  |
| Local  | 0xA0000000 | 0x40000000 | 1G |
| Remote | 0xB0000000 | 0x80000000 | 1G |

#### C2C 
| Block | USP base address | 7s base address | size |
| ---   | ---      | ---     | ---  |
| CM1 AXI | 0xB0000000 | 0x80000000 | 16M |
| CM1 AXI-Lite | 0xB1000000 | 0x81000000 | 16M |
| CM2 AXI | 0xB2000000 | 0x82000000 | 16M |
| CM2 AXI-Lite | 0xB3000000 | 0x83000000 | 16M |
