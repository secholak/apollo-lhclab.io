# CPLD FW

## Overview
The Apollo SM board has a CoolRunner II CPLD on it used to provide flexible connections between components on the board.

The firmware and CI outputs for the CPLD are located [here](https://gitlab.com/apollo-lhc/FW/SM_Rev2_CPLD_FW)

## Updating the FW

The CPLD can be programmed via the Zynq, but the IOs for this are disabled by default.
The XVC for this is under the system daemon

Ex:
```
[dan@tesla SM_Rev2_CPLD_FW]$ ./program.py --HW_SERV_PORT 9991 --xvc_ip apollo208-1 --xvc_port 2545 --jed_file top.jed 
source /work/Xilinx/Vivado/2020.2/settings64.sh;hw_server -s tcp::9991
source /work/Xilinx/Vivado/2020.2/settings64.sh;vivado -mode tcl  -source /tmp/98a74e29-0ea7-4500-b330-0d688ea2e65e_xvc.tcl

****** Xilinx hw_server v2020.2
  **** Build date : Nov 18 2020 at 09:50:49
    ** Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.

INFO: hw_server application started
INFO: Use Ctrl-C to exit hw_server application

INFO: To connect to this hw_server instance use url: TCP:tesla:9991

Warning: Cannot create '3000:arm' GDB server: Address already in use
Warning: Cannot create '3001:arm64' GDB server: Address already in use
Warning: Cannot create '3002:microblaze' GDB server: Address already in use
Warning: Cannot create '3003:microblaze64' GDB server: Address already in use

****** Vivado v2020.2 (64-bit)
  **** SW Build 3064766 on Wed Nov 18 09:12:47 MST 2020
  **** IP Build 3064653 on Wed Nov 18 14:17:31 MST 2020
    ** Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.

source /tmp/98a74e29-0ea7-4500-b330-0d688ea2e65e_xvc.tcl
# open_hw_manager
# connect_hw_server -quiet -verbose -url localhost:9991
# open_hw_target -quiet -xvc_url apollo208-1:2545
# puts "xvc running"
source /work/Xilinx/ise/14.7/ISE_DS/settings64.sh; impact -batch /tmp/4299d974-61e4-4c96-bc99-b8489095afd3_ise.cmd
. /work/Xilinx/ise/14.7/ISE_DS/common/.settings64.sh /work/Xilinx/ise/14.7/ISE_DS/common
. /work/Xilinx/ise/14.7/ISE_DS/EDK/.settings64.sh /work/Xilinx/ise/14.7/ISE_DS/EDK
. /work/Xilinx/ise/14.7/ISE_DS/PlanAhead/.settings64.sh /work/Xilinx/ise/14.7/ISE_DS/PlanAhead
. /work/Xilinx/ise/14.7/ISE_DS/ISE/.settings64.sh /work/Xilinx/ise/14.7/ISE_DS/ISE
Release 14.7 - iMPACT P.20131013 (lin64)
Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
Preference Table
Name                 Setting             
StartupClock         Auto_Correction     
AutoSignature        False               
KeepSVF              False               
ConcurrentMode       False               
UseHighz             False               
ConfigOnFailure      Stop                
UserLevel            Novice              
MessageLevel         Detailed            
svfUseTime           false               
SpiByteSwap          Auto_Correction     
AutoInfer            false               
SvfPlayDisplayComments false               
INFO:iMPACT - Connecting to hw_server...
INFO:iMPACT - Connection established.
Identifying chain contents...'0': : Manufacturer's ID = Xilinx xc2c64a, Version : 0
INFO:iMPACT:1777 - 
   Reading /work/Xilinx/ise/14.7/ISE_DS/ISE/xbr/data/xc2c64a.bsd...
INFO:iMPACT:501 - '1': Added Device xc2c64a successfully.
----------------------------------------------------------------------
----------------------------------------------------------------------
done.
Elapsed time =      0 sec.
Elapsed time =      0 sec.
Elapsed time =      0 sec.
Elapsed time =      0 sec.
'1': Loading file 'top.jed' ...
done.
INFO:iMPACT:501 - '1': Added Device xc2c64a successfully.
----------------------------------------------------------------------
----------------------------------------------------------------------
Maximum TCK operating frequency for this device chain: 0.
Validating chain...
Boundary-scan chain validated successfully.
'1': Erasing device...
'1': Erasure completed successfully.
'1': Programming device...
done.
'1': Verifying device...
done.
'1': Setting ISC done bits...done
'1': Programming completed successfully.
Elapsed time =      1 sec.
Done programming
Shutting down Vivado
Shutting down hw_serv
```

## Verifying the update

```
>read SERV.CPLD.FIRMWARE.DATA
                           SERV.CPLD.FIRMWARE.DATA: 0x5833E913
>read SERV.CPLD.FIRMWARE.DATA_VALID
                     SERV.CPLD.FIRMWARE.DATA_VALID: 0x00000001
```

Understanding the protocol
```
>read SERV.CPLD.FIRMWARE.RAW_DATA_*
                     SERV.CPLD.FIRMWARE.RAW_DATA_0: 0xFFFFFFFF
                     SERV.CPLD.FIRMWARE.RAW_DATA_1: 0x5833E913
                     SERV.CPLD.FIRMWARE.RAW_DATA_2: 0x00000000
                     SERV.CPLD.FIRMWARE.RAW_DATA_3: 0x55555555
```