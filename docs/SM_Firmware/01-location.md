# Location:

The SM Zynq FW is located in [gitlab](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/) and has recently (2023) moved to [Hog](https://cern.ch/hog) for its build system.

