# Address Table Documentation

The address table specifies the list of registers that `BUTool` can perform read+write operations, using the implementations in the `Apollo` device plugin. These files are of XML format, and each entry describes a single register with a given memory address, read+write permissions, and other `BUTool`-related parameters.

Detailed information on address table parameters is available on `BUTool` manual. <a href="https://bu-edf.gitlab.io/BUTool/BUToolDocs/AddressTable/Overview/" target="_blank">This</a> section gives an overview of the possible node attributes. Detailed documentation on the `StatusDisplay` related parameters can be found <a href="https://bu-edf.gitlab.io/BUTool/BUToolDocs/AddressTable/Parameters/" target="_blank">here</a> in `BUTool` manual.