# Home

<img src="images/apollo.png" title="lhc apollo logoo" width="200" />

This is a project to develop a common ATCA blade which can be used for readout
and triggering applications in LHC experiments. It is named after the Apollo
program CSM spacecraft which separated into Command and Service modules.

Apollo is used in the CMS Tracker and in ATLAS L0MDT.

<apollo-blade.info/>

## Quick Links
* This Document's source: <https://gitlab.com/apollo-lhc/apollo-lhc.gitlab.io>
* Service Module
    * Hardware SVN: <http://gauss.bu.edu/svn/common-atca-blade.hardware>
    * Document SVN: <http://gauss.bu.edu/svn/common-atca-blade>
    * Firmware: <https://github.com/apollo-lhc/SM_ZYNQ_FW>
* Command Module
    * Hardware Git: <https://github.com/apollo-lhc/Cornell_CM_Rev1_HW>
    * Microcontroller Firmware: <https://github.com/apollo-lhc/cm_mcu>
    * FPGA Firmware: <https://github.com/apollo-lhc/CM_FPGA_FW>
* IPMC
    * Open IPMC HW: <https://gitlab.com/openipmc/openipmc-hw>
    * Open IPMC FW: <https://gitlab.com/BU-EDF/openipmc-fw/>
    * CERN IPMC FW: <https://github.com/apollo-lhc/apollo-cern-ipmc>
* Software
    * BUTool Software: <https://github.com/apollo-lhc/ApolloTool>
    * Grafana Monitor: <https://github.com/apollo-lhc/Grafana-Monitor>
    * apollo-herd: <https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd>

## Introduction
Is there something missing from the documentation?  If so, please file a ticket here <https://gitlab.com/apollo-lhc/apollo-lhc.gitlab.io/-/issues>


