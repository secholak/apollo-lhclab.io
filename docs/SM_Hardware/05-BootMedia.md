#Boot Media

## SDCard

**Note:** You will likely need `sudo` for all of the commands described below.

This section covers how to set up the microSD card for booting. To configure the SD card, attach it to a USB device, and attach the USB device
to a computer. You can find the corresponding device file by looking at the latest daemon messages:

```bash
# Look at the last entries to see which device file is used for the USB connector
dmesg
```

You should see a device file called `/dev/sdX` in the logs. For the remainder of these instructions, all commands such as `fdisk` will be executed
on this device file. 

#### Erase the first sector
```bash
dd if=/dev/zero of=/dev/sdX bs=1024 count=1
```
#### Partition the card

Create one 1GB FAT32 partition for the boot files, and the remainder as Linux ext4 for the filesystem. This is done by `fdisk` in Linux.

```bash
fdisk /dev/sdX
```
(delete any existing partitions; shouldn't be any)

#### Now create partitions:

On the `fdisk` console, do the following to create the two partitions:

```bash
Command (m for help): n
Partition type:
 p primary (0 primary, 0 extended, 4 free)
 e extended
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-15759359, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-15759359, default 15759359): +1G
  
Command (m for help): n
Partition type:
 p primary (1 primary, 0 extended, 3 free)
 e extended
Select (default p): p
Partition number (1-4, default 2): 2
First sector (411648-15759359, default 411648):
Using default value 411648
Last sector, +sectors or +size{K,M,G} (411648-15759359, default 15759359):
Using default value 15759359
Set bootable flag, partition types:

Command (m for help): a
Partition number (1-4): 1
  
Command (m for help): t
Partition number (1-4): 1
Hex code (type L to list codes): c
Changed system type of partition 1 to c (W95 FAT32 (LBA))
  
Command (m for help): t
Partition number (1-4): 2
Hex code (type L to list codes): 83
Check table and write changes:

Command (m for help): p
  
Disk /dev/sdb: 8068 MB, 8068792320 bytes
249 heads, 62 sectors/track, 1020 cylinders, total 15759360 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x920c958b
  
 Device Boot Start End Blocks Id System
/dev/sdb1 * 2048 411647 204800 c W95 FAT32 (LBA)
/dev/sdb2 411648 15759359 7673856 83 Linux
  
Command (m for help): w
The partition table has been altered!
  
Calling ioctl() to re-read partition table.
  
WARNING: If you have created or modified any DOS 6.x
partitions, please see the fdisk manual page for additional
information.
Syncing disks.
```

#### Create filesystems

After `fdisk` commands from previous section are completed, you should see two partitions, `/dev/sdX1` and `/dev/sdX2`. Now, create filesystems for both partitions using the following commands:

```bash
mkfs.vfat -F 32 -n boot /dev/sdX1
mkfs.ext4 -L root /dev/sdX2
```
#### Fill the filesystems

After the filesystems are created, both partitions need to be filled with the proper file content. The first FAT32 partition will have the boot file contents, and the second partition will have the Linux filesystem. For both of these, tarballs are used which can be extracted when a file partition is mounted. The tarballs for the filesystem and boot files can be found as artifacts of the GitLab CI/CD jobs in their respective repos. Filesystem build pipelines can be found [here](https://gitlab.com/apollo-lhc/soc-os/-/pipelines),
and the boot file/firmware builds are [here](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/pipelines). Please make sure to get the firmware and filesystem builds for the microSD card, **not the EMMC**.

Once you get the tarballs, fill the first partition, as described in the steps below. 

* Mount the first partition to `/mnt`

```bash
# Mount the first partition to /mnt
mount /dev/sdX1 /mnt
cd /mnt
```

* Extract the firmware tarball retrieved from GitLab CI/CD jobs. If you encounter warning messages about file permissions, you can ignore it. 

```bash
# Extract the rev2a_xczu7ev.tar.gz file
tar -p -zxf rev2a_xczu7ev.tar.gz

```

* For the MAC address of the Zynq, add a file called `eth1_mac.dat`, to store the MAC address of the `eth1` interface on Zynq. At BU, the MAC address convention is `00:50:51:FF:10:<boardNumberInHex>`, where `boardNumberInHex`
is the serial number of the Service Module in hex. For example, for SM 203, the expected MAC address would be `00:50:51:FF:10:CB`.

```bash
# Create the eth1_mac.dat file
touch eth1_mac.dat

# Open the file with your favorite text editor and save the MAC address content as explained in above text 
vim eth1_mac.dat
```

* After all done, unmount the partition.

```bash
# Finally unmount
cd ..
umount /mnt
```

Similarly, fill the second partition:

```bash
# Mount the first partition to /mnt
mount /dev/sdX2 /mnt
cd /mnt

# Extract the rev2a_xczu7ev.tar.gz file (this time, obtained from the filesystem CI/CD)
tar --numeric-owner -p -zxf rev2a_xczu7ev.tar.gz

# Untarring of the filesystem may take a while...

# Finally unmount, again, this might take a while due to the sync between the SD card and the cache
cd ..
umount /mnt
```

Now, the SD card is all set with both partitions filled, and is ready to be tested for a boot on an Apollo Service Module!

## EMMC

The instructions are almost identical to the configuration of the SD card, explained in the previous section. Once Zynq booted from the SD card, you should see
the device file called `/dev/mmcblk0`, representing the EMMC. This should be configured by creating two partitions, `/dev/mmcblk0p1` for the FAT32 boot files and
`/dev/mmcblk0p2` for the Linux filesystem.

Connect to Zynq via SSH, note that you should be root user for the following operations:

```bash
# DNS name is apollo2xx-1 at BU, 2xx being the serial number of the Apollo rev2a board
ssh root@apollo2xx-1
```

Once connected, you can erase the contents on the device file first:

```bash
dd if=/dev/zero of=/dev/mmcblk0 bs=1024 count=1
```

Then you can configure the two partitions and make the filesystems, identical to the procedure explained above for the SD cards:

```bash
# Create the two partitions, see instructions above for the SD card, it's identical
fdisk /dev/mmcblk0

...

# Create the filesystems
mkfs.vfat -F 32 -n boot /dev/mmcblk0p1
mkfs.ext4 -L root /dev/mmcblk0p2
```

And getting the EMMC firmware/filesystem tarballs from the CI/CD jobs ([here](https://gitlab.com/apollo-lhc/soc-os/-/pipelines) for the filesystem and
[here](https://gitlab.com/apollo-lhc/FW/SM_ZYNQ_FW/-/pipelines) for the firmware/boot files), extract the tarballs in each partition. For the first partition,
remember to create the `eth1_mac.dat` file, identical to the first partition for the SD card.

First partition:
```bash
# Mount the first partition to /mnt
mount /dev/mmcblk0p1 /mnt
cd /mnt

# Extract the rev2a_xczu7ev.tar.gz file
tar -p -zxf rev2a_xczu7ev.tar.gz

# Create the eth1_mac.dat file
touch eth1_mac.dat

# Open the file with your favorite text editor and save the MAC address content as explained earlier 
# (identical to the instructions for the SD card)
#
# MAC address convention @ BU is -> 00:50:51:FF:10:<boardNumberInHex>
vim eth1_mac.dat

# Finally unmount
cd ..
umount /mnt
```

Second partition:
```bash
# Mount the first partition to /mnt
mount /dev/mmcblk0p2 /mnt
cd /mnt

# Extract the rev2a_xczu7ev.tar.gz file
tar --numeric-owner -p -zxf rev2a_xczu7ev.tar.gz

# Finally unmount
cd ..
umount /mnt
```

Now you have the EMMC configured and ready for boot!

