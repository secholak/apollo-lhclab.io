# Plan for CI/CD Development

This section represents a plan for the necessary tasks to develop a fully-automated CI/CD testing workflow.

## OpenIPMC FW CI/CD

### Necessary Steps

To test the OpenIPMC FW builds, one would need to do the following: 

* Designate an Apollo Service Module (SM) for CI/CD related use
* Retrieve the `upgrade.hpm` file (or similar) from the build
* Upload the `hpm` file to the IPMC of the designated SM
* Once the upgrade+activation steps are complete, need to find out:
    * If the IPMC successfully booted up with the new FW
    * (if the IPMC is up) Can write the necessary values to the EEPROM and check if the values are correctly written
    * I2C registers can also be read to make sure that certain values (e.g. ZYNQ MAC addresses) are properly written to ZYNQ's I2C interface
    * Finally, check if the ZYNQ is up with it's expected IP address

### Status

The status of the tasks outlined above are as follows:

| Task | Status |
| ---- | ------ |
| Retrieving `hpm` file  | <span style="color:orange">Need to implement in the current CI/CD</span> |
| Upgrading IPMC FW      | <span style="color:green">Python3 script available</span> |
| Checking if IPMC is up | <span style="color:red">No script available</span>      |
| Writing to & checking EEPROM values | <span style="color:green">Python3 script available</span> |
| Checking I2C register values | <span style="color:red">No script available</span> |
| Checking if ZYNQ is up | <span style="color:red">No script available</span> |

IPMC related scripts for FW upgrade and EEPROM read/writes can be found [here](https://github.com/alpakpinar/ipmc_scripts). For a full CI/CD workflow, these scripts would be needed to be pulled by the CI job, and be executed there.