# Front Panel

## Upper Green LED Array

### Running Zynq is indicated by "Cylon" LED animation

<img width=200 src="../img/MDTTP-FP-cylon.svg">


### Zynq waiting to start is indicated by this LED animation

<img width=200 src="../img/MDTTP-FP-waiting.svg">


