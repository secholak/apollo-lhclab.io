# Working At BU

## Getting Started

This section will cover:

* How to get computer accounts on necessary machines
* How to connect to Apollo boards
* How to connect to OpenIPMCs

### Computer Accounts

To access Apollo boards located at BU, you will first need a computer account on the Tesla machine.
Please contact BU-EDF engineer [Dan Gastler](https://www.bu.edu/physics/profile/daniel-gastler/) for an
account on this machine.

Once you have the computer account at Tesla, you can SSH with your username:

```bash
ssh <username>@tesla.bu.edu
```

There is a second `server-room` computer, which is **only** reachable within the BU-EDF internal network. You can also use this computer if you want to run SHEP front-end web application. Please contact either [Alp Akpinar](https://www.bu.edu/physics/profile/alp-akpinar/) or [Dan Gastler](https://www.bu.edu/physics/profile/daniel-gastler/) for an
account on this machine.

**Note:** If you're not going to run SHEP, you won't specifically need an account on the `server-room` machine, you can use Tesla instead.

Once you have the computer account to `server-room`, you can log in as such:

```bash
# First log in to Tesla to connect to EDF internal network
ssh <username>@tesla.bu.edu

# From Tesla, hop on to connect to the server-room machine
ssh <username>@server-room
```

### Connecting to Apollo Boards

The list of currently active Apollo blades in BU and their IP addresses can be found [here](https://ohm.bu.edu/~apollo/shelf_status.html). From the Tesla computer, you can SSH to Apollo boards with `cms` and `atlas` users. Please contact members of the BU group to receive passwords for these accounts. 

For example, to connect to the SM 207 (Host name: `apollo207-1`) with the `cms` user, you can do:

```bash
ssh cms@apollo207-1
```

### Connecting to OpenIPMCs

The list of active OpenIPMCs (at BU) and their IP addresses can be found under the same shelf status page mentioned earlier, [here](https://ohm.bu.edu/~apollo/shelf_status.html). From the Tesla computer you can use `telnet` to access the command line interface of OpenIPMCs.

For example, to connect to `openipmc0037`, you can do:

```bash
telnet openipmc0037
```

For more information on OpenIPMCs, please see the following links within the Apollo manual:

* [Terminal commands](https://apollo-lhc.gitlab.io/IPMC/01-ipmc-terminal/)
* [Firmware upgrade](https://apollo-lhc.gitlab.io/IPMC/05-ipmc-firmware-upgrade/)
* [FAQ](https://apollo-lhc.gitlab.io/IPMC/08-ipmc-faq/)

### Running SHEP on Server-room Machine

Please see [here](https://apollo-lhc.gitlab.io/Software/apollo-herd/) for description of the SHEP framework and instructions to run SHEP.