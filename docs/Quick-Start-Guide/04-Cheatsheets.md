# Cheat-sheets, Tips and Tricks

## Updating the SM
  - [SM Firmware updating](../../SM_FW/05-Updating_Firmware)
  - SM filesystem updating __ADD DOC__

## Programming the CM
  - [One-shot](../../Command-Module/04-FW_Loading_internals/#loading-cm-firmware-in-one-go)

## Slot Numerology


| Physical Slot | Logical Slot | IPMB-L Address |
| ------------- | ------------ | -------------- |
| 1  | 13 | 0x9A |
| 2  | 11 | 0x96 |
| 3  | 9  | 0x92 |
| 4  | 7  | 0x8E |
| 5  | 5  | 0x8A |
| 6  | 3  | 0x86 |
| 7  | 1  | 0x82 |
| 8  | 2  | 0x84 |
| 9  | 4  | 0x88 |
| 10 | 6  | 0x8C |
| 11 | 8  | 0x90 |
| 12 | 10 | 0x94 |
| 13 | 12 | 0x98 |
| 14 | 14 | 0x9C |


## Hardware inventory at BU

Click [**here**](http://ohm.bu.edu:8000/hardware)

## MPI CM specific info

### Access the MCU console

From ``BUTool``, enter ``uart_term CM_1``
    
### Programming the Kintex
   
* Warning!  When reprogramming an FPGA on a running CM, first
  ``cmpwrdown`` and then ``cmpwrup`` in BUTool

* After powering up the CM, enable C2C communications	
```
cd /home/atlas/mdttp-cm-demo-mcu-main/Software/HwTest/pyMcu
./script/setup_sm-cm.sh
```

* Program the Kintex from ``BUTool`` (example svf file shown here)

```
svfplayer /fw/CM/MPI_rev1_p1_KU15p-2-SM_7s/bit/top_MPI_rev1_p1_KU15p-2-SM_7s.svf
unblockAXI
```


