# Glossary

**ATCA** Advanced Telecommunications and Computing Architecture

**AXI** Advanced eXtensible Interface

**C2C** Chip-to-Chip

**CM** Command Module

**FP** front panel

**FPGA** Field Programmable Gate Array

**GBT** GigaBit Transceiver

**HAL** Hardware Abstraction Layer

**IPMB** Intelligent Platform Management Bus

**IPMC** IPMI Management Controller

**JTAG** Joint Test Action Group

**lpGBT** low power GBT

**MDTTP** ATLAS MDT Trigger Processor

**SM** Service Module

**SoC** System-on-Chip

**SoM** System-on-Module

**svf** serial vector format

**UART** Universal Asynchronous Receiver-Transmitter

**uHAL** uHAL is the hardware access library used by the IPBus software

**XVC** Xilinx Virtual Cable