
# EyeScans

Xilinx documentation for the GTH transceiver eyescan lives 
[here](https://docs.xilinx.com/v/u/en-US/ug576-ultrascale-gth-transceivers) in 
Ch. 4, RX Margin Analysis.

## Getting Started
The command for running an eyescan from BUTool is
```
>es <horz_increment> <vert_increment> <max_prescale> <base_node> <lpm_node> <output_file>
```
The horizontal and vertical increments can be any positive integer below a 
board-dependent value. Increments of 5 are a reasonable starting place.

The max prescale determines the achievable precision for high Bit Error Rate 
(BER) regions. A large prescale increases the precision of these areas at the 
cost of speed. The prescale can be from 0 to 31. 0 is a reasonable starting 
point.

The base node is the DRP node of the link you want scanned, and lpm node is the 
corresponding LPM enable node. For example
```
CM.CM_1.C2C_1.DRP
```
```
CM.CM_1.C2C_1.DEBUG.RX.LPM_EN
```

## Eyescan Hardware Implementation
Xilinx implements the eyescan as the state machine shown here.

![Image](figs/ES_state_machine.png)

An eyescan measurment is performed in the count state. The GTH, GTY, GTX 
transceivers have a second sampler in parallel with the data sampler which 
records the state of the signal at an offset from the data 
sampler. If the value at the offset differs from that recorded by the data 
sampler, a count is added to an error counter. A sample counter also records 
the total samples taken. Once either counter saturates, the scan completes, and 
the counts can be read to obtain the BER.

In Low-Power Mode (lpm)

![Image](https://latex.codecogs.com/svg.image?\text{BER}=\frac{\text{error count}}
{\text{sample count}\times2^{1+\text{prescale}}\times\text{data width}})

In Decision Feedback Equalizer mode (DFE), two separate scans are needed which 
accumulate to two separate error counts. 

![Image](https://latex.codecogs.com/svg.image?\text{BER}=\frac{\text{error0}+
\text{error1}}{\text{sample count}\times2^{1+\text{prescale}}\times\text{data width}})

The prescale allows for the count recorded for sample count to be scaled. This 
can be useful if the sample count saturates with very few errors counted. 
Increasing the prescale decreases the number of samples taken per sample count 
recorded, so more errors can be counted before the sample counter saturates.

## Eyescan Software Implementation
The software sets up and runs eyescans on a grid of offsets to produce a full 
eyescan plot. The software is part of 
[ApolloSM_plugin](https://gitlab.com/apollo-lhc/software/ApolloSM_plugin). It 
is split into two parts: ApolloSM_device, and eyescan. ApolloSM_device handles 
the BUTool command, and controls the eyescan class, which is an eyescan on a 
single link.
![Image](figs/legacy_es_device.drawio.svg)

At its core, the eyescan class is an FSM. A pixel is a measurement at a 
specified offset. The states SCAN_START and SCAN_PIXEL are responsible for 
interfacing with the hardware to perform a scan. The state is changed between 
SCAN_START and SCAN_PIXEL until all the pixels have been iterated through and 
scanned.

![Image](figs/legacy_eyescan-update.drawio.svg)

### Realignment
When error is detected at the offset origin (offset sampler at data sampler 
position), a realignment sequence is performed.

[source](https://support.xilinx.com/s/article/66517?language=en_US), 
[source](https://support.xilinx.com/s/article/68785?language=en_US)
> The realignment sequence has been introduced for Production silicon and 
>datarates below 10Gbps. The aim of this sequence is to get a correct sync 
>between the Eye Scan clock and the data clock. 
> 
> In case of bad synchronization, the error counter will saturate and the Eye 
>Scan will show a closed eye.
> 
> The sequence should be executed at the end of the Eye Scan measurement if 
>the eye appears completely closed, even if there are no data errors.
> 
> The sequence below moves the Eye Scan clock in 2 UI increments. The 
>realignment sequence is as follows:
> 
>     ES_HORZ_OFFSET = x880
>     EYESCANRESET = 1
>     ES_HORZ_OFFSET = x800
>     EYESCANRESET = 0
