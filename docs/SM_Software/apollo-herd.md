# Apollo-HERD SWATCH Plugin
The [apollo-herd](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/tree/master) software registers Apollo-specific controls to the [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch) (SoftWare for Automating conTrol of Common Hardware) framework, by wrapping SWATCH code to call [ApolloSM](https://github.com/apollo-lhc/ApolloSM_plugin) functions.

This software is meant to be run as a Docker container only, using the `start_apolloherd.sh` script located in the root of the `apollo-herd` repository, and a specific Docker image URL to make a container. The documentation on how to run the `apollo-herd` software on Apollo hardware can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation#apollo).

Documentation for more specific code internals is also available [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/tree/dev/release/v2.0/docs). This documentation describes the following:

* YAML configuration files to be used by the `herd` command line application
* Classes defined within `apollo-herd` to represent devices
* Implementation of device-specific commands and registering them to the `SWATCH` framework
* The GitLab CI process to build the Docker images for `apollo-herd`

# Shep-HERD Framework overview
This section gives an overall view of the `SHEP` + `HERD` framework, where `SHEP` is the UI front-end application to interact with the `apollo-herd` back-end application, that is running on the Apollo blades. Using the `SHEP` UI application, users can monitor data coming from registered `Apollo` devices, and issue commands. These commands will be executed on the hardware, using the functionality implemented in `apollo-herd` software.

Source code for the `SHEP` UI application can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep). In practice, the `SHEP` application is run as a set of Docker containers, using the `docker-compose` utility. There are different containers for the UI, API server, the SQL database, and so on. Instructions to run the `SHEP` application as such can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation#running-shep).

## Tracker Online Software stack
![Image](figs/shepHERDcrook.png)

The [Tracker Online Software stack](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw) provides the infrastructure for deployment of remote monitoring and control software for CMS' Phase-2 Tracker boards. It employs a three-tiered design to delegate control of the hardware:

* **HERD:** Hardware-specific HERD plugins (like apollo-herd) run in Docker containers on the board's SoC. The HERD plugin library provides the framework for registration of commands and FSM transitions based on those commands to the SWATCH framework, for invocation by the Shep server. 

* **SHEP:** The SHEP layer describes a set of off-board supervisor applications, each of which supervises a group of boards via their HERD plugins. Boards are registered to the SHEP framwork, and then a web application oversees command executions across those boards. The documentation to use the SHEP UI can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/deployment-and-documentation/-/blob/master/Shep%20User%20Guide.md). 

* **Crook:** The highest level of the framework, the Crook is a single instance which allows for global control of all boards through the deployment and orchestration of the Shep servers. 
