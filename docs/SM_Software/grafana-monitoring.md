# Grafana Monitoring

Default parameters of `Graphite` keep the collected data only for 6h.
This can be changed in:

     <installation folder>/conf/storage-schemas.conf

Important to note that, if the data acquisition has already started, changing the configuration file above is not enough.
In this case, the related `.wsp` files should be updated.
The location of this files change depending on your installation.
Usual places are `/var/lib/graphite` and `/opt/graphite/storage`.
An example of command to update these files and keep data for a year is:

    find $d -iname "*.wsp" -exec /opt/graphite/bin/whisper-resize.py {} 10s:365d \;
