# Linux Daemons (Services)

This section gives an overview of some of the crucial Linux daemons that will be running on the background (upon boot) on the Zynq. All daemons will be launched
automatically by `systemd` when the Zynq is booting, and some daemons depend on others to start. Hence, an overall knowledge of the daemons and their
functionalities/dependencies is useful.

## Daemons

### UIO Daemon

UIO daemon is responsible for loading the UIO devices upon boot and creating
the address table and connections XML files under `/opt/address_table`. This address table provides an interface for `BUTool` and related software to
communicate with the Apollo Service and Command Modules. This service also launches a server that listens on a UNIX socket under `/tmp/uio_socket`,
which can be used to load overlay devices and program FPGAs on the Command Module, please see [here](https://apollo-lhc.gitlab.io/Command-Module/04-FW_Loading_internals/#uio-client)
for those instructions. 

* **Install location:** /opt/uio-daemon
* **Configuration file:** /etc/uio_daemon
* **Log location:** /var/log/uio_daemon.log

Check if the service is running:

```bash
systemctl status uio_daemon.service
```

**Note:** Please note that the UIO daemon is typically a requirement for most other daemons that are launched later in the boot sequence. The reason for this is that
the next daemons in line will use the address table, set up by the UIO daemon. Hence, if UIO daemon fails, most other services (including the ones described below)
will not be launched.

### I2C Write Monitor

This is a simple service, which is tasked with reading the `SLAVE_I2C.S1.SM.STATUS.I2C_DONE` register periodically, until it reads a value of `1`. 
A value of `1` indicates that during the OpenIPMC boot process, the OpenIPMC is finished writing data to the `SLAVE1` I2C interface of the Zynq. As soon as this
service reads a `1`, it informs `systemd` that it is in OK state, and allows it to launch next services in the dependency queue. If it does not read
`1` for 60 seconds (default timeout value), it times out and still allows `systemd` to proceed with next services.

* **Install location:** /opt/BUTool/bin/i2c_write_monitor
* **Configuration file:** /etc/i2c_write_monitor
* **Log location:** Logs to `syslog`. This is equivalent to `/var/log/messages` file on CentOS7 and AlmaLinux8 systems.

Check if the service is running:

```bash
systemctl status i2c_write_monitor.service
```

Get the logs of this service:

```bash
journalctl -u i2c_write_monitor.service
```

### Netconfig Daemon

Netconfig daemon is responsible for setting up the ethernet interface (`eth1` on rev2a SMs) of the Zynq upon boot. It does that by reading the MAC address
upon boot, from one of the three sources (in order):

* I2C registers of Zynq, written by the OpenIPMC with the proper MAC address
* `/fw/SM/eth1_mac.dat` file (for `eth1` interface)
* The default MAC address specified for this port in the configuration file, `/etc/netconfig_daemon`

Please see [here](https://apollo-lhc.gitlab.io/Service-Module/zynq-eth-interface/#networking-daemon) for more detailed information.

* **Install location:** /opt/netconfig-daemon/netconfig_daemon.py
* **Configuration file:** /etc/netconfig_daemon
* **Log location:** /var/log/netconfig_daemon.log

Check if the service is running:

```bash
systemctl status netconfig_daemon.service
```

### Netup Daemon

Netup daemon checks whether the ethernet interface configured by the preceding netconfig daemon is set up properly. It does so by checking the content of
`/proc/net/route` file. If the ethernet is OK, the daemon will allow `systemd` to pass on to dependent services such as the DHCP client service (please see below).

* **Install location:** /opt/netconfig-daemon/netup_daemon.py
* **Configuration file:** None
* **Log location:** /var/log/netup_daemon.log

Check if the service is running:

```bash
systemctl status netup_daemon.service
```

### DHCP Daemon

After the netconfig and netup daemons are done, this service runs `dhclient` for Zynq to obtain an IP address, based on its ethernet address.

* **Install location:** The `.service` file is under `/opt/netconfig-daemon/dhclient.service`. It does not have source code since it just executes `dhclient`.
* **Configuration file:** None
* **Log location:** Logs to `syslog`. This is equivalent to `/var/log/messages` file on CentOS7 and AlmaLinux8 systems.

Check if the service is running:

```bash
systemctl status dhclient.service
```

Get the logs of this service:

```bash
journalctl -u dhclient.service
```

## Troubleshooting

This section includes some debugging tips for common problems that might occur during boot process of the Zynq.

### The network did not come up

Since the network interface of Zynq is not up, you'll need to `minicom` to the Zynq. You can attach a USB cable to the front-panel board of the blade, and check
what device file is assigned on the computer using `dmesg`. You should see a file like `/dev/ttyUSBX`, where `X` is some integer. You can then launch the `minicom`
session:

```bash
# Find out which device file under /dev you need from dmesg first
minicom -D /dev/ttyUSBX
```

The console will prompt you for a login, please log in with user+password info. For the below debugging tips to work, please log in as `root` user, since you
might be launching executables under `/opt` and you will need permissions.

Once you are connected to the blade, check: Are the services prior to the `dhclient.service` (which is tasked with getting an IP address) all running?
In majority of the cases, some prior daemon fails and `dhclient.service` is simply not launched. Check for:

* I2C write monitor 
* UIO daemon 
* Netconfig + netup daemons

Please see above for `systemctl status` commands to check these. 

Once you locate which service has failed, first try reading the logs of the service to see if there is a clear failure. You can find how to read this info for
each service in the above section.

If this does not work, try stopping the service via `systemctl` and run it interactively. For example if the UIO daemon failed, you can do:

```bash
# Stop the problematic service, so that systemd does not start to restart it again and again
systemctl stop uio_daemon.service

# Launch it interactively and see how it crashes
# NOTE: The path is /opt/uio-daemon in older filesystems
cd /opt/uio-daemon/python
python3 uio_daemon.py --nodaemon

# See the problem..
```

Some common problems can be a missing dependency, or an old configuration file. Once you fix that, please execute `reboot` and try again.

### Zynq boots up (LEDs look OK) but it takes a while until I can ping it

This can be due to an OpenIPMC not writing MAC address data to Zynq via I2C. In this scenario, the I2C write monitor (see above) will poll the
`SLAVE_I2C.S1.SM.STATUS.I2C_DONE` register until it times out in 60 seconds, before launching the other services. The netconfig daemon will still be
able to read the MAC address from `/fw/SM/eth1_mac.dat` (this is why **it is important to have this file**), 
so eventually the Zynq will have an IP address. It just takes extra time because of the 1 minute
timeout by the I2C write monitor service.

This behavior is not expected with a recent OpenIPMC, but if it persists, it might be worthwile to either check the state of the 
I2C bus between OpenIPMC and Zynq using a logic analyzer (Saleae), or run a debugger session on the OpenIPMC to see why it is not sending data over.

### OpenIPMC boots up but Zynq does not boot up at all

This is probably not related to daemons, but you might have other problems like having the front panel handles open. In that case the OpenIPMC
will not send a bootup request to Zynq. This might be the case if you are seeing three red LEDs going on in sequence ("Handle Open" case in 
[here](https://apollo-lhc.gitlab.io/IPMC/03-ipmc-leds/)). Please make sure that the handles are tightly shut.

Another scenario where this will happen is if you do not have the front-panel switch plugin connected to the Service Module, so that OpenIPMC does not
know about handles being closed (please see [here](https://apollo-lhc.gitlab.io/Assembly/cm-sm-assembly/#front-end-panel-assembly) in the assembly instructions).
